#Geek Squad Town Hall Rick Roll Demo#

Arduino demo shown for Programmer's Day at the 2017-09-13 Geek Squad Town Hall.

Originally performed on an Arduino Uno clone with a simple buzzer with its ground connected to the Arduino ground and its positive side connected to Pin 9 through a 100ohm resistor.

Adapted from the Arduino PlayMelody sample: https://www.arduino.cc/en/Tutorial/PlayMelody