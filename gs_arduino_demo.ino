void full_console_output();
void play_tone(int tone_length, int duration);
void play_complex_song();

#define PIN_SPEAKER_OUT 9

#define A04 2272
#define F04 2863
#define G04 2551
#define AS4 2145
#define C05 1911
#define D05 1703
#define  R0 0

int g_melody[] =
{ 
  F04, G04, AS4, G04,
  D05,  R0, D05, C05,  R0, F04, G04, AS4, G04,
  C05,  R0, C05, AS4,  R0, F04, G04, AS4, G04,
  AS4, C05, A04, F04,  R0, C05, AS4
};

int g_beats[] =
{
  8, 8, 8, 8,
  32, 32, 16, 32, 2000,  8,  8,  8,  8,
  32, 32, 16, 16, 2000,  8,  8,  8,  8,
  32, 16, 64, 16, 0, 32, 32
};

bool display_set = false;
long g_tempo = 16000;
long g_pause = 500;
int g_rest_count = 100;
int g_melody_size = (sizeof(g_melody)/2);

//
// setup() - Runs once before loop() begins
//
void setup() {
  Serial.begin(9600);
  Serial.println("Serial debugging enabled.");
  pinMode(PIN_SPEAKER_OUT, OUTPUT);
}

//
// loop() - Runs infinitely as long as the board is powered up
//
void loop() {
  (void)play_complex_song();

  delay(g_pause);
}

void play_tone(int tone_length, int duration) {
  long elapsed_time = 0;
  long tone_duration = (duration * g_tempo);
  long tone_length_half = (tone_length/2);

  if(0 < tone_length) {
    while(elapsed_time < tone_duration) {
      //Serial.print(tone_length); Serial.print(" "); Serial.println(duration);
      digitalWrite(PIN_SPEAKER_OUT, HIGH);
      delayMicroseconds(tone_length_half);
      digitalWrite(PIN_SPEAKER_OUT, LOW);
      delayMicroseconds(tone_length_half);
      elapsed_time += tone_length;
    }
  } else {
    for (int index = 0; g_rest_count > index; index++) {
      delayMicroseconds(duration);
    }
  }
}

void play_complex_song() {
  (void)full_console_output();
    
  for(int index=0; g_melody_size>index; index++) {
    (void)play_tone(g_melody[index], g_beats[index]);
  }
}

void full_console_output(){
  if (true == display_set) { return; }
  
  Serial.println("                    ++###++");
  Serial.println("                 '++#######+");
  Serial.println("                 ###+++++++'.");
  Serial.println("               ++###++';;;;;;");
  Serial.println("               ++###++''';;;.");
  Serial.println("               +++++++''''''");
  Serial.println("               '''''''''''''");
  Serial.println("                 +''''''';;;");
  Serial.println("                 ''''''''''");
  Serial.println("                 '''''+''''");
  Serial.println("             ++++''''''''");
  Serial.println("          ++#####;''''';;++'");
  Serial.println("     ''+#########;''''';;+++##++'");
  Serial.println("   ''############;..;;;;;+++#####");
  Serial.println("  ;###############''''+++++'#####");
  Serial.println("  ;###############++'''####'#####''");
  Serial.println("  ;####@@@##########'''####;#####++");
  Serial.println("  +#####@@@@@#######'''####'#####++");
  Serial.println("  ######@@@@@@@#####+++############");
  Serial.println("  ###@@@@@@@@@@@@@@@##+############");
  Serial.println("''#####@@@@@@@@@@@@@++'''++########");
  Serial.println("''#@@@@@@@@@@@@@@@++++'''##########");
  Serial.println("''#@@@@@@@@@@@@@@@++++'''#######@##");
  Serial.println("++#@@@@@@@@@@@@@@@+++++#########@@@##");
  Serial.println("  #@@@@@@@@@@@@@@@@@@@@@@#######@@@@@@##++'");
  Serial.println("  #@@@@@@@@@@@@##@@@##@@@#####@@@@@@@@@@+++++++'''");
  Serial.println("        ##@@@@@@@@@@@@@@@#####@@#@@@@@@@+++++'''''");
  Serial.println("        ##@@@@@@@@@@@@@@@###@@@@;;;++#@@###'';;'");
  Serial.println("       '####@@@@@@@@@@@++###@@@@");
  Serial.println("       '##@@@@@@@@@@@@@++###@@@@");
  Serial.println("");
  Serial.println("Never gonna give you up, never gonna let you down");
  Serial.println("Never gonna run around and desert you   - Rick Astley");
  display_set = true;
}
